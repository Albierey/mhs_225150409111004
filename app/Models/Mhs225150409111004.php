<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mhs225150409111004 extends Model
{
    use HasFactory;
    protected $table = 'mhs_225150409111004';
}
