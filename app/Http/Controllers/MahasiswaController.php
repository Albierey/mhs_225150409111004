<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mhs225150409111004;

class MahasiswaController extends Controller
{
    public function index()
    {
        $mahasiswa = Mhs225150409111004::all();
        return view('mahasiswa.index', compact('mahasiswa'));
    }

    public function store(Request $request)
    {
        $mahasiswa = new Mhs225150409111004;
        $mahasiswa->nim = $request->input('nim');
        $mahasiswa->nama = $request->input('nama');
        $mahasiswa->save();
        return redirect()->route("mahasiswa.index");
    }
}