<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMhs225150409111004Table extends Migration
{
    public function up()
    {
        Schema::create('mhs_225150409111004', function (Blueprint $table) {
            $table->id();
            $table->string('nim');
            $table->string('nama');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('mhs_225150409111004');
    }
}
