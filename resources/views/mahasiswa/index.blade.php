<!DOCTYPE html>
<html>
<head>
    <title>Data Mahasiswa</title>
</head>
<body>
    <h1>Data Mahasiswa</h1>

    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Created At</th>
                <th>Updated At</th>
            </tr>
        </thead>
        <tbody>
            @foreach($mahasiswa as $mhs)
            <tr>
                <td>{{ $mhs->id }}</td>
                <td>{{ $mhs->nim }}</td>
                <td>{{ $mhs->nama }}</td>
                <td>{{ $mhs->created_at }}</td>
                <td>{{ $mhs->updated_at }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <form method="POST" action="{{ route('mahasiswa.store') }}">
        @csrf
        <label for="nim">NIM:</label>
        <input type="text" name="nim" id="nim">
        <br>
        <label for="nama">Nama:</label>
        <input type="text" name="nama" id="nama">
        <br>
        <button type="submit">Tambah Mahasiswa</button>
    </form>
</body>
</html>